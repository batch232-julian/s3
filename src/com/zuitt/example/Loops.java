package com.zuitt.example;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Loops {
    public static void main(String[] args){
//        Loops are control structure that allow code blocks to be separated according to the conditions
//        Types of loops:
//        While loop
//        int a = 1;
//
//        while (a < 5) {
//            System.out.println("While loop counter:" + a);
//            a++;
//        }
//        Scanner userInput = new Scanner(System.in);
//        boolean hasNoInput = true;
//        while (hasNoInput){
//            System.out.println("Enter your name:");
//            String name = userInput.nextLine();
//
//            if (name.isEmpty()){
////                System.out.println("Please try again");
////            }else{
////                hasNoInput = false;
////                System.out.println("Thank you for your input");
////            }
////        }

//        Do-while loop
        int b = 5;

        do {
            System.out.println("Countdown: "+ b);
            b--;
        }while (b >= 1);

//        for loop
        for (int i = 0; i<= 10; i++){
            System.out.println("Count:" + i);
        }

//        for loop over a java array
        int[] intArray = {100, 200, 300, 400, 500};
        for(int i = 0; i < intArray.length; i++){
            System.out.println("Item at  index number "+ i + " is " + intArray[i]);
        }
//        Multidimensional Array
//        firs array could be the rows, second array could be the cols
        String[][] classroom = new String[3][3];

//        first row
        classroom[0][0] = "Tony";
        classroom[0][1] = "Steve";
        classroom[0][2] = "Bruce";

//        second row
        classroom[1][0] = "Natasha";
        classroom[1][1] = "Wanda";
        classroom[1][2] = "Clint";

//        third row
        classroom[2][0] = "Scott";
        classroom[2][1] = "Peter";
        classroom[2][2] = "Stephen";

//        Nested for loop
        for (int row = 0; row < 3;  row++){
            for (int col = 0; col < 3; col++){
                System.out.println(classroom[row][col]);
            }
        }
        System.out.println(Arrays.deepToString(classroom));

//        enhanced for loop for arrays and array list
        String[] members = {"Eugene", "Vincent", "Dennis", "Alfred"};
//        member is a parameter represnting an item in the memners array, even asa a parameter, you have to indicate the data type
        for (String member: members){
            System.out.println(member);
        }
//        Enhance for loop in multidimenssional array
//        for(String row: classroom){
//            for (String student: row)
//
//
//
//            }
//        }

//        for each for hashMap
    HashMap<String, String> techniques = new HashMap<>();
    techniques.put(members[0], "Spirit Gun");
    techniques.put(members[1], "Black Dragon");
    techniques.put(members[2], "Rose Whip");
    techniques.put(members[3], "Spirit Sword");
    System.out.println(techniques);

    techniques.forEach((key, value)-> {

        System.out.println("Member "  + key + " uses " + value);
        });


    }
}

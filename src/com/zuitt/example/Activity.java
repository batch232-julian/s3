package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {

//        Hashmap and contents ---------------------
        String[] listOfPokemon = {"Mario Odyssey", "Super Smash Bros. Ultimate", "Luigi's Mansion 3", "Pokemon Sword", "Pokemon Shield"};

        HashMap<String,Integer> pokemonStock = new HashMap<>();
        pokemonStock.put(listOfPokemon[0],50);
        pokemonStock.put(listOfPokemon[1],20);
        pokemonStock.put(listOfPokemon[2],15);
        pokemonStock.put(listOfPokemon[3],30);
        pokemonStock.put(listOfPokemon[4],100);

        pokemonStock.forEach((key,value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });


//      Adding topGames -------------------------
        ArrayList<String> topGames = new ArrayList<>();
        pokemonStock.forEach((key,value) -> {
            if(value <= 30){
                System.out.println(key + " has been added to the List of Top Games");
                topGames.add(key);
            }
        });
        System.out.println("Our shop's top games:");
        System.out.println(topGames);


//        Stretch Goal  ------------------
      boolean addPokemon = true;
      Scanner itemScanner = new Scanner(System.in);
      while(addPokemon){
          System.out.println("Would you like to add an item? Yes or No");
          String input = itemScanner.nextLine();

          switch(input){
              case "Yes":
                  System.out.println("Add Item Name");
                  String item = itemScanner.nextLine();

                  System.out.println("Add Number of Stocks");
                  Integer numberStocks = itemScanner.nextInt();

                  pokemonStock.put(item,numberStocks);
                  System.out.println(item + " has been added with " + numberStocks + " stocks");
                  addPokemon = false;
                  break;
              case "No":
                  System.out.println("Thank you.");
                  addPokemon = false;
                  break;
              default:
                  System.out.println("Invalid input. Try Again");
            }
        }

    }
}
